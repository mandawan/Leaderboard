//client only code

Template.leaderboard.helpers({
  players: function () {
    return Players.find({}, { sort: { score: -1, name: 1 } });
  },
  selectedName: function () {
    var player = Players.findOne(Session.get("selectedPlayer"));
    return player && player.name;
  }
});

Template.leaderboard.events({
  'click .inc': function () {
    Players.update(Session.get("selectedPlayer"), {$inc: {score: 5}});
  },
  'click .del': function(){
    Meteor.call('removePlayer', Session.get("selectedPlayer"));
  }
});

Template.player.helpers({
  selected: function () {
    return Session.equals("selectedPlayer", this._id) ? "selected" : '';
  }
});

Template.player.events({
  'click': function () {
    Session.set("selectedPlayer", this._id);
  }
});


// Inside the if (Meteor.isClient) block, right after Template.body.helpers:
Template.newplayer.events({
  "submit .new-player": function (event) {
    // This function is called when the new task form is submitted

    var name = event.target.text.value;

    Players.insert({
      name: name,
      score: 0
    });

    // Clear form
    event.target.text.value = "";

    // Prevent default form submit
    return false;
  }
});

